# Overview #
Develop a simple program that calculates the price of a shopping basket.

### Description ###
The output shall be displayed similar to what you would expect to see on a receipt.
Items are presented one at a time, in a list, identified by name - for example "Apple" or �Banana".

Given a shopping list with items, calculate the total cost of those items.
The basket can contain any item multiple times. Items are priced as follows:

* Apples are 25 ct. each.
* Oranges are 30 ct. each.
* Bananas are 15 ct. each.
* Papayas are 50 ct. each but are available as 'three for the price of two'.

Keep the exercise as simple as possible, good luck!

### Prerequisites ###
In order to build and run the app you must have the following software installed in your machine:

* Java 8
* Maven 3.3 or higher

### Building and running ###
Use the following command to build the application

```
mvn clean package
```

Run the app

```
mvn test
```