package shoppingcart;

import java.util.UUID;

/*
 * @author Stavros Nicolakopoulos
 *
 * @since Dec 1, 2018
 *
 */

public class CartItem extends Product {
	
	private long id;
	
	private double discountPrice;

	public CartItem(String name, double price) {
		super(name, price);
	}
	
	public CartItem(Product product) {
		super(product.getName(), product.getPrice());
		id = UUID.randomUUID().hashCode();
	}

	public double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(double discountPrice) {
		this.discountPrice = discountPrice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartItem other = (CartItem) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CartItem [id=" + id + ", discountPrice=" + discountPrice + ", name=" + getName() + ", price="
				+ getPrice() + "]";
	}
	
	
}
