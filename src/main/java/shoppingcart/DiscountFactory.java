package shoppingcart;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

/*
 * @author Stavros Nicolakopoulos
 *
 * @since Dec 1, 2018
 *
 */

public class DiscountFactory {
	
	private static Map<String, DiscountPolicy> discounts = new HashMap<>();
	
	// ---------- Lambda functions
	
	private static BiFunction<Product, Integer, Double> two4one = (product, quantity) -> quantity % 2 == 0 ? product.getPrice() : 0.0;
	
	private static BiFunction<Product, Integer, Double> three4two = (product, quantity) -> quantity % 3 == 0 ? product.getPrice() : 0.0;
	
	// ---------- In memory storage
	
	static {
		Optional<Product> banana = Inventory.getProduct("Bananas");
		banana.ifPresent(p -> addDiscount(p.getName(), "TwoForOne", two4one));
		
		Optional<Product> papaya = Inventory.getProduct("Papayas");
		papaya.ifPresent(p -> addDiscount(p.getName(), "ThreeForTwo", three4two));
	}
	
	// ---------- Public API
	
	public static void addDiscount(String productName, String discountName, BiFunction<Product, Integer, Double> calc) {
		discounts.put(productName, new DiscountFactory.DiscountPolicy(discountName, calc));
	}
	
	public static Double getDiscountPrice(Product product, int quantity) {
		DiscountPolicy policy = discounts.get(product.getName());
		return policy == null ? 0.0 : policy.getCalculator().apply(product, quantity);
	}
	
	private static class DiscountPolicy {
		
		private String name;
		
		BiFunction<Product, Integer, Double> calculator;

		public DiscountPolicy(String name, BiFunction<Product, Integer, Double> calculator) {
			super();
			this.name = name;
			this.calculator = calculator;
		}

		public String getName() {
			return name;
		}

		public BiFunction<Product, Integer, Double> getCalculator() {
			return calculator;
		}
		
	}
	
	// ---------- Printing
	
	private static final String line = new String(new char[30]).replace('\0', '-');
	private static final String titleFormat = "| %-26s |";
	private static final String discountFormat = "| %-12s | %-11s |";
	
	public static void printDiscounts() {
		print(line);
		print(String.format(titleFormat, "Avalable discounts"));
		print(line);
		print(String.format(discountFormat, "Product", "Discount"));
		print(line);
		discounts.forEach((k, v) -> print(String.format(discountFormat, k, v.getName())));
		print(line);
		print("");
	}
	
	private static void print(String s) {
		System.out.println(s);
	}
	
}
