package shoppingcart;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/*
 * @author Stavros Nicolakopoulos
 *
 * @since Dec 1, 2018
 *
 */

public class Inventory {
	
	private final static List<Product> products = new ArrayList<>();
	
	// ---------- In memory storage
	
	static {
		products.add(new Product("Apples", 0.25));
		products.add(new Product("Oranges", 0.3));
		products.add(new Product("Bananas", 0.15));
		products.add(new Product("Papayas", 0.5));
	}
	
	// ---------- Public API

	public static List<Product> getProducts() {
		return products;
	}
	
	public static Optional<Product> getProduct(String name) {
		return products.parallelStream()
				.filter(p -> p.getName().equals(name))
				.findFirst();
	}
	
	// ---------- Printing
	
	private static final String line = new String(new char[24]).replace('\0', '-');
	private static final String titleFormat = "| %-20s |";
	private static final String productFormat = "| %-12s | %-5s |";
	
	public static void printProducts() {
		print(line);
		print(String.format(titleFormat, "Avalable products"));
		print(line);
		print(String.format(productFormat, "Product", "Price"));
		print(line);
		products.forEach(p -> print(String.format(productFormat, p.getName(), p.getPrice())));
		print(line);
		print("");
	}
	
	private static void print(String s) {
		System.out.println(s);
	}

}
