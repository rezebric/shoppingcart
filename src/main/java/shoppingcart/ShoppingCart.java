package shoppingcart;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/*
 * @author Stavros Nicolakopoulos
 *
 * @since Dec 1, 2018
 *
 */

public class ShoppingCart {
	
	private final List<CartItem> cardItems = new LinkedList<>();
	private final Map<String, Integer> itemQuantities = new HashMap<>();
	
	// ---------- Lambda functions
	
	private final BiFunction<String, Integer, Integer> increaseQuantity = (k, v) -> v + 1;
	private final BiFunction<String, Integer, Integer> decreaseQuantity = (k, v) -> v - 1;
	
	private final Function<Double, Double> roundUp = value -> new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	
	private final Consumer<CartItem> addItemToCart = item -> cardItems.add(item);
	private final Consumer<CartItem> removeItemFromCart = item -> cardItems.remove(item);
	
	private final Consumer<CartItem> increaseItemQuantity = item -> { 
		Integer quantity = itemQuantities.computeIfPresent(item.getName(), increaseQuantity);
		if(quantity == null) itemQuantities.put(item.getName(), 1);
	};
	
	private final Consumer<CartItem> decreaseItemQuantity = item -> {
		itemQuantities.computeIfPresent(item.getName(), decreaseQuantity);
	};
	
	private final Consumer<CartItem> applyDiscount = item -> {
		Integer quantity = itemQuantities.get(item.getName());
		Optional<Product> product = Inventory.getProduct(item.getName());
		product.ifPresent(p -> item.setDiscountPrice(DiscountFactory.getDiscountPrice(p, quantity)));
	};
	
	private final Function<Product, CartItem> addToCart = product -> {
		CartItem item = new CartItem(product);
		addItemToCart.andThen(increaseItemQuantity).andThen(applyDiscount).accept(item); 
		return item;
	};
	
	private final Consumer<CartItem> removeFromCart = item -> {
		removeItemFromCart.andThen(decreaseItemQuantity).accept(item);
	};
	
	// ---------- Public API
	
	public ShoppingCart() {
		print("*********** New ShoppingCart ***********");
		print("");
	}
	
	public CartItem addItem(Product product) {
		print("Adding " + product.getName());
		return addToCart.apply(product);
	}
	
	public void removeItem(CartItem item) {
		print("Removing " + item.getName());
		removeFromCart.accept(item);
	}
	
	public Optional<CartItem> getItem(CartItem item) {
		return Optional.of(cardItems.get(cardItems.indexOf(item)));
	}
	
	public  List<CartItem> getCardItems() {
		return this.cardItems;
	}
	
	public int getSize() {
		return cardItems.size();
	}
	
	public Double computeTotalPrice() {
		double result = cardItems.stream().mapToDouble(s -> s.getPrice()).sum();
		return roundUp.apply(result);
	}
	
	public Double computeDicountTotalPrice() {
		double result = cardItems.stream().mapToDouble(s -> s.getDiscountPrice()).sum();
		return roundUp.apply(result);
	}
	
	public Double computeFinalPrice() {
		double result = computeTotalPrice() - computeDicountTotalPrice();
		return roundUp.apply(result);
	}
	
	// ---------- Printing
	
	private static final String line = new String(new char[40]).replace('\0', '-');
	private static final String titleFormat = "| %-36s |";
	private static final String productFormat = "| %-15s | %-5s | %-10s |";
	private static final String totalFormat = "| %-15s | %-18s |";
	
		
	public void printItems() {
		double totalPrice = computeTotalPrice();
		double totalDiscount = computeDicountTotalPrice();
		double finalPrice = computeFinalPrice();
		print(line);
		print(String.format(titleFormat, "Shopping cart"));
		print(line);
		print(String.format(productFormat, "Product", "Price", "Discount"));
		print(line);
		cardItems.forEach(item -> print(String.format(productFormat, item.getName(), item.getPrice(), item.getDiscountPrice())));
		print(line);
		print(String.format(totalFormat, "Total price", totalPrice));
		print(String.format(totalFormat, "Total discount", totalDiscount));
		print(String.format(totalFormat, "Final price", finalPrice));
		print(line);
		print("");
	}
	
	private static void print(String s) {
		System.out.println(s);
	}

}
