package shoppingcart;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/*
 * @author Stavros Nicolakopoulos
 *
 * @since Dec 1, 2018
 *
 */

public class TestShoppinCart {
	
	ShoppingCart shoppingCart;
	
	Optional<Product> apple;
	Optional<Product> orange;
	Optional<Product> banana;
	Optional<Product> papaya;
	
	Optional<CartItem> appleItem;
	Optional<CartItem> orangeItem;
	Optional<CartItem> bananaItem;
	Optional<CartItem> papayaItem;
	
	Function<Product, CartItem> addItem = product -> shoppingCart.addItem(product); 
	Consumer<Optional<CartItem>> removeItem = item -> item.ifPresent(it -> shoppingCart.removeItem(it));
	
	@BeforeAll
	public static void info() {
		Inventory.printProducts();
		DiscountFactory.printDiscounts();
	}
	
	
	@BeforeEach
	public void initShoppingCart() {
		
		apple = Inventory.getProduct("Apples");
		orange = Inventory.getProduct("Oranges");
		banana = Inventory.getProduct("Bananas");
		papaya = Inventory.getProduct("Papayas");
		
		shoppingCart = new ShoppingCart();
		
		appleItem = apple.map(addItem);
		orangeItem = orange.map(addItem);
		bananaItem = banana.map(addItem);
		papayaItem = papaya.map(addItem);
		
		shoppingCart.printItems();
		
		assertTrue(shoppingCart.getSize() == 4);
	}
	
	@Test
	public void testRemoveItem() {
		
		removeItem.accept(papayaItem);
		
		shoppingCart.printItems();
		
		assertTrue(shoppingCart.getSize() == 3);
	}
	
	@Test
	public void testTwoForOne() {
		
		 Optional<CartItem> bananaItem2 = banana.map(addItem);
		 
		 shoppingCart.printItems();
		 
		 assertTrue(bananaItem2.get().getName().equals("Bananas"));
		 assertTrue(bananaItem2.get().getPrice() == 0.15);
		 
		 assertTrue(shoppingCart.getSize() == 5);
		 assertTrue(shoppingCart.computeTotalPrice() == 1.35);
		 assertTrue(shoppingCart.computeDicountTotalPrice() == 0.15);
		 assertTrue(shoppingCart.computeFinalPrice() == 1.2);
	}
	
	@Test
	public void testThreeForTwo() {
		
		 Optional<CartItem> papayaItem2 = papaya.map(addItem);
		 Optional<CartItem> papayaItem3 = papaya.map(addItem);
		 Optional<CartItem> papayaItem4 = papaya.map(addItem);
		 Optional<CartItem> papayaItem5 = papaya.map(addItem);
		 Optional<CartItem> papayaItem6 = papaya.map(addItem);
		 
		 shoppingCart.printItems();
		 
		 assertTrue(papayaItem2.get().getName().equals("Papayas"));
		 assertTrue(papayaItem2.get().getPrice() == 0.5);
		 
		 assertTrue(papayaItem3.get().getName().equals("Papayas"));
		 assertTrue(papayaItem3.get().getPrice() == 0.5);
		 
		 assertTrue(papayaItem4.get().getName().equals("Papayas"));
		 assertTrue(papayaItem4.get().getPrice() == 0.5);
		 
		 assertTrue(papayaItem5.get().getName().equals("Papayas"));
		 assertTrue(papayaItem5.get().getPrice() == 0.5);
		 
		 assertTrue(papayaItem6.get().getName().equals("Papayas"));
		 assertTrue(papayaItem6.get().getPrice() == 0.5);
		 
		 assertTrue(shoppingCart.getSize() == 9);
		 assertTrue(shoppingCart.computeTotalPrice() == 3.7);
		 assertTrue(shoppingCart.computeDicountTotalPrice() == 1.0);
		 assertTrue(shoppingCart.computeFinalPrice() == 2.7);
	}
	

}
